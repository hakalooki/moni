<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

Class Signup extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');

		$this->load->library('session');

		$this->load->model('login_mdl');
	}

	public function index() {
		$this->load->view('signup-view');
		
	}


	public function user_login() {

		$d['status']='empty';

		//$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		/*if ($this->form_validation->run() == FALSE) {
			//$this->logout();

			if(isset($this->session->userdata['logged_in'])){
				$this->load->view('import-dir', $d);
			} else {
				$this->load->view('login-form');
			}
		} else {*/
			$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
			);

			$result = $this->login_mdl->login($data);

			if ($result == TRUE) {

				$username = $this->input->post('username');
				$result = $this->login_mdl->user_information($username);
				if ($result != false) {
					$sess_data = array(
					'username' => $result[0]->username,
					//'email' => $result[0]->email,
					'email' => 'idea@idea.com',
					'mode' => mode,
					'fchk' => fchk
					);

					if(fchk == false) return false;

					$this->session->set_userdata('logged_in', $sess_data);
					$this->load->view('import-dir', $d);
				}
			} else {
				
				$data = array(
				'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('login-form', $data);
			}
		//}


		//var_dump($this->session->userdata['logged_in']['username']);
		//var_dump($result);
	}

	public function logout() {
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('import');
		$this->session->unset_userdata('importx');
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('login-form', $data);
	}

}

?>