<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

Class Crvs extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');

		$this->load->library('session');

		$this->load->model('docs_mdl');
	}

	public function index() {
		$data['docs'] = $this->docs_mdl->listAll();
		$this->load->view('dash-crvs', $data);
		
	}

	public function get_docs(){
    $docs = $this->docs_mdl->listAll();
    echo json_encode($docs);
    //echo ($docs->result_array());
	}

	public function getFilePath(){
		if($this->uri->segment(3))
		{
			$path='files/cvrs/';
		}
		$name = $this->uri->segment(3);
		return $path;
	}

	public function down_doc(){
    $this->load->helper('download');
		if($this->uri->segment(3))
		{
			$data = file_get_contents('./files/cvrs/'.$this->uri->segment(3));
		}
		$name = $this->uri->segment(3);
		force_download($name, $data);
	}

	public function delete_doc($id) {
	  if ($this->docs_mdl->delete_doc($id))
	  {
	      $status = 'success';
	      $msg = 'File successfully deleted';
	  }
	  else
	  {
	      $status = 'error';
	      $msg = 'Something went wrong when deleteing the file, please try again';
	  }
	  echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function file_upload() {
		if(isset($_FILES["userFile"]["name"])){
			$config['upload_path'] = './files/cvrs/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|txt';
			$config['encrypt_name'] = TRUE;
			//$config['max_size'] = 1024 * 8;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('userFile')){
				$error = $this->upload->display_errors();
				echo $error;
			}
			else{
				$data = $this->upload->data();
				$doc = $this->docs_mdl->create($data["file_name"]);
				echo $data["file_name"];
				//var_dump($_POST['doc_title']) ;
			}
		}
	}

}

?>