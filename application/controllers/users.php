<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

Class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');

		$this->load->library('session');

		$this->load->model('users_mdl');
	}

	public function index() {
		$data['users'] = $this->users_mdl->get_users();

		$this->load->view('users-view', $data);
	}

	public function get_users() {
		$result = $this->users_mdl->get_users();

		var_dump($result);
		
	}

	public function page($p='', $org) {
		$data['pg'] = $p;
		if($p=='tasks')
			$data['rs'] = $this->tasks_mdl->listAllByOrg($org);

		if($p=='subtasks')
			$data['rs'] = $this->subtasks_mdl->listAllByOrg($org);

		$this->load->view('passport-view', $data);
	}

	function user_entry()
	{
		$data['pg'] = 'addusers';
		$data['status']='';
		
		if($this->uri->segment(3)){
			$data['status'] = $this->uri->segment(3);
		}
		
		$data['org_records'] = $this -> users_mdl -> listAllSelect('organisations','id','name','organisation');
		//var_dump($data);
		//$data['aname_records'] = $this -> fees_model -> all_select_list('sub_account_info','account_code','account_name');
		
		/*$data['user_type'] = $this -> tank_auth -> get_usertype();
		$data['user_name'] = $this -> tank_auth -> get_username();
		$data['main_content'] = 'superadmin/accounts/fees_entry_view';
		$data['tricker_content'] = 'include/left_sidebar_accounts';*/
		$this->load->view('users-view', $data);
	}

	function create_user(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'User Email', 'required');
		
		if($this->form_validation->run() == FALSE){
			redirect('users/user_entry/error');
		}
		else{
			$exists = $this->users_mdl->redundancy_check('users','email',$this->form_validation->set_value('email'));
			
			if(!$exists){
				$this->users_mdl->create();
				redirect('users/user_entry/success');
			}
			else{
				redirect('users/user_entry/exists');
			}
		}
	}


}

?>