<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

Class Passport extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');

		$this->load->library('session');

		$this->load->model('tasks_mdl');
		$this->load->model('subtasks_mdl');
	}

	public function index() {
		//$this->load->view('passport-view');
		
	}


	public function passport_page($p='progress', $org) {
		$data['pg'] = $p;
		if($p=='progress')
			$data['rs'] = $this->tasks_mdl->taskProgressByOrg($org);
		
		if($p=='tasks')
			$data['rs'] = $this->tasks_mdl->listAllByOrg($org);

		if($p=='subtasks')
			$data['rs'] = $this->subtasks_mdl->listAllByOrg($org);

		$this->load->view('passport-view', $data);
	}

	function task_entry()
	{
		$data['pg'] = 'addtasks';
		$data['status']='';
		
		if($this->uri->segment(3)){
			$data['status'] = $this->uri->segment(3);
		}
		
		//$data['bank_records'] = $this -> fees_model -> all_select_list('bank_info','bank_account_code','bank_name');
		//$data['aname_records'] = $this -> fees_model -> all_select_list('sub_account_info','account_code','account_name');
		
		/*$data['user_type'] = $this -> tank_auth -> get_usertype();
		$data['user_name'] = $this -> tank_auth -> get_username();
		$data['main_content'] = 'superadmin/accounts/fees_entry_view';
		$data['tricker_content'] = 'include/left_sidebar_accounts';*/
		$this->load->view('passport-view', $data);
	}


	function create_tasks(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('task_name', 'Task Name', 'required');
		
		if($this->form_validation->run() == FALSE){
			redirect('passport/task_entry/error');
		}
		else{
			$exists = $this->tasks_mdl->redundancy_check('tasks','name',$this->form_validation->set_value('task_name'));
			
			if(!$exists){
				$this->tasks_mdl->create();
				redirect('passport/task_entry/success');
			}
			else{
				redirect('passport/task_entry/exists');
			}
		}
	}

	function subtask_entry()
	{
		$data['pg'] = 'addsubtasks';
		$data['status']='';
		
		if($this->uri->segment(3)){
			$data['status'] = $this->uri->segment(3);
		}
		
		$data['task_records'] = $this -> subtasks_mdl -> listAllSelectByOrg('tasks','id','name', '1');
		//var_dump($data);
		//$data['aname_records'] = $this -> fees_model -> all_select_list('sub_account_info','account_code','account_name');
		
		/*$data['user_type'] = $this -> tank_auth -> get_usertype();
		$data['user_name'] = $this -> tank_auth -> get_username();
		$data['main_content'] = 'superadmin/accounts/fees_entry_view';
		$data['tricker_content'] = 'include/left_sidebar_accounts';*/
		$this->load->view('passport-view', $data);
	}

	function create_subtasks(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subtask_name', 'Subtask Name', 'required');
		
		if($this->form_validation->run() == FALSE){
			redirect('passport/subtask_entry/error');
		}
		else{
			$exists = $this->tasks_mdl->redundancy_check('subtasks','name',$this->form_validation->set_value('subtask_name'));
			
			if(!$exists){
				$this->subtasks_mdl->create();
				redirect('passport/subtask_entry/success');
			}
			else{
				redirect('passport/subtask_entry/exists');
			}
		}
	}

	public function putProgress() {   
    	$data = array(
	        'table_name' => $this->input->post('tname'),
	        'id' => $this->input->post('id'),
	        'progress' => $this->input->post('progress')
    	);

	    if($this->tasks_mdl->putProgress($data))
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }

	}


	function task_progress(){
		$data['rs'] = $this->tasks_mdl->taskProgressByOrg(1);
		var_dump($data);
		
			/*redirect('passport/subtask_entry/error');
			$exists = $this->tasks_mdl->redundancy_check('subtasks','name',$this->form_validation->set_value('subtask_name'));
			
			if(!$exists){
				$this->subtasks_mdl->create();
				redirect('passport/subtask_entry/success');
			}
			else{
				redirect('passport/subtask_entry/exists');
			}*/
	}

}

?>