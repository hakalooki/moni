<?php
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

Class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');

		$this->load->library('session');

		$this->load->model('login_mdl');
	}

	public function index() {
		$this->load->view('login-view');
		
	}


	public function user_login() {

		$d['status']='empty';

		//$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		/*if ($this->form_validation->run() == FALSE) {
			//$this->logout();

			if(isset($this->session->userdata['logged_in'])){
				$this->load->view('import-dir', $d);
			} else {
				$this->load->view('login-form');
			}
		} else {*/
			$data = array(
			'mobileOrMail' => $this->input->post('mobileOrMail'),
			'password' => $this->input->post('password')
			);

			$result = $this->login_mdl->login($data);

			if ($result == TRUE) {

				$mobileOrMail = $this->input->post('mobileOrMail');
				$result = $this->login_mdl->user_information($mobileOrMail);
				if ($result != false) {
					$sess_data = array(
					'name' => $result[0]->name,
					'surname' => $result[0]->surname,
					'email' => $result[0]->email,
					'org' => $result[0]->organisation,
					'role' => $result[0]->role,
					'mode' => mode,
					'fchk' => fchk
					);

					if(fchk == false) return false;

					$this->session->set_userdata('logged_in', $sess_data);

					$rl = $this->session->userdata['logged_in']['role'];

					if (isset($rl)) {
					  switch ($rl) {
					    case "A":
					      $this->load->view('dash-admin', $d);
					      break;
					    case "PA":
					      $this->load->view('passport-view', $d);
					      break;
					    /*case "subtasks":
					      $this->load->view('dash-admin', $d);
					      break;*/
					    default:
					      $this->load->view('dash-admin', $d);
					  }
					}

					/*if($this->session->userdata['logged_in']['role'] == 'A')
						$this->load->view('dash-admin', $d);
					else
						$this->load->view('question-view', $d);*/
				}
			} else {
				
				$data = array(
				'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('login-view', $data);
			}
		//}


		//var_dump($this->session->userdata['logged_in']['username']);
		//var_dump($result);
	}


	public function load_admin_dashboard() {
		if($this->session->userdata['logged_in']['role'] == 'A')
			$this->load->view('dash-admin');
		else
			$this->load->view('login-view');
	}

	public function logout() {
		/*$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('import');
		$this->session->unset_userdata('importx');
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('login-view', $data);*/

		$this->load->view('login-view');
	}

}

?>