<div id="mainApp">


  <div id="mainContent">

    <div id="contentArea">
      <routehandler componentName="{{state.nav.activeView || 'emptypage'}}"/>
    </div>

    <div id="mainNav">
      <div class="tabs is-vertical has-block-icons">
        <ul>
          {{#state.mainSections:k}}
            {{#visible !== false}}<li class="{{state.nav.activeTab == k ? 'is-active' : ''}}"><a href="#{{url}}"><icon icon="{{icon}}"/> {{k}}</a></li>{{/}}
          {{/state.mainSections}}
        </ul>
      </div>
    </div>

    <div id="header" class="nav">
      <div class="nav-left logo-area">
        <div class="nav-item logo-holder">
          <img class="logo" src="images/logo-symbol-es.png">
        </div>
        <div class="nav-item current-property-name text">
          <span>
            {{selectedPropertyName}}
          </span>
        </div>
        {{#if state.properties.length>1}}
          <div class="nav-item property-name-dropdown">
            <div class="dd-icon text">
              <icon icon="caret-down" size="small"></icon>
              <select value="{{state.selectedProperty}}">
                <option value="">Select a property...</option>
                {{#each state.properties}}
                  <option value="{{id}}">{{name}}</option>
                {{/each}}
              </select>
            </div>
          </div>
        {{/if}}
      </div>

      <div class="nav-right is-flex-mobile">
        <div class="nav-item text">
          <div class="media">
            <div class="media-left">
              {{#state.user.avatar}}
                <figure class="image is-32x32"><img src="{{state.user.avatar}}"></figure>
              {{/}}
            </div>
            <div class="media-content">
              <span class="is-block" style="line-height:1.3rem">{{state.user.name}} {{state.user.surname}}</span>
              <small class="is-block"><span class="tag is-warning"><a href="#profile/">My Profile</a></span>&nbsp;|&nbsp;<span class="tag is-dark">{{state.user.role}}</span></small>
            </div>
          </div>
        </div>
        <div class="nav-item">
          <a class="button is-danger" on-tap="logout"><icon icon="sign-out"/> <span class="is-hidden-mobile">Logout</span></a>
        </div>
      </div>
    </div>

    <div id="modals">

    </div>
  </div>
</div>

<script>
  component.exports = {}
</script>