<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          <span class="icon is-large">
            <i class="fa fa-book fa-3x" aria-hidden="true"></i>
          </span>
        </div>

        <div class="media-content">
          <h1 class="title">Passport</h1>
          <h2 class="subtitle">All subtasks under your account</h2>
        </div>
      </div>
    </div>

    <div class="column">
      <div class="tabbar is-fullwidth">
        <div class="tabs is-right is-boxed">
          <ul>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/progress/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
                <span>Progress</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/tasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-list" aria-hidden="true"></i></span>
                <span>Tasks</span>
              </a>
            </li>
            <li class="is-active">
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/subtasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-bars" aria-hidden="true"></i></span>
                <span>Subtasks</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="columns" style="margin:10px 30px">
  <div class="column">
    <form on-submit="dosearch">
      <div class="field has-addons">
        <p class="control is-expanded">
          <input class="input" type="text" placeholder="{{placeholder}}" value="{{query}}">
        </p>

        <p class="control">
          <button class="button is-info" type="submit"><span class="icon is-small"><i class="fa fa-search" aria-hidden="true"></i></span> <span>Search</span></button>
        </p>
      </div>
    </form>
  </div>
  <div class="column is-narrow">
    <a href="<?php echo base_url();?>index.php/passport/subtask_entry" class="button is-success is-pulled-right"><span class="icon is-small"><i class="fas fa-plus-circle"></i></span> <span>Add Subtasks</span></a>
  </div>
</div>



<div id="" class="columns">
  <div class="column data-table" style="margin:5px 50px">
    <table class="table is-striped is-bordered is-hoverable is-fullwidth">
      <tr>
        <th style="width:50px;">ID</th>
        <th style="width:500px;">Name</th>
        <th style="width:600px;">Description</th>
        <th style="width:400px;">Task</th>
        <th></th>
      </tr>
      <?php foreach($rs as $field): ?>
      <tr>
        <td><?php echo $field->id; ?></td>
        <td><?php echo $field->name; ?></td>
        <td><?php echo $field->description; ?></td>
        <td><?php echo $field->task; ?></td>
        <td>
          <a class="edit-icon" href="#users/view/{{id}}">
            <span class="icon is-small">
              <i class="fas fa-pencil-alt"></i>
            </span>
          </a>
        </td>
      </tr>
      <?php endforeach; ?>
    </table>
    
  </div>


</div>


<div id="" class="columns">

</div>