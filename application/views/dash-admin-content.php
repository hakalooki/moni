<div id="" class="columns is-full">
<div id="" class="column is-12">

  <nav class="panel" relative="true" style="margin: 50px;">
  <p class="panel-heading">
    Work Progress
  </p>
  <div class="columns">
      <div class="column">
           <div class="card">
              <header class="card-header">
                <p class="card-header-title">
                  Passport
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
              </header>
              <div class="card-content has-text-centered">
                <div class="content subtitle is-1">
                  27%
                  <p class="subtitle is-4">bbbbbbb</p>
                </div>
              </div>
          
            </div>
      </div>

      <div class="column">
           <div class="card">
              <header class="card-header">
                <p class="card-header-title">
                  ORG
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
              </header>
              <div class="card-content has-text-centered">
                <div class="content subtitle is-1">
                  63%
                  <p class="subtitle is-4">mmmmmmmm</p>
                </div>
              </div>
          
            </div>
      </div>

      <div class="column">
           <div class="card">
              <header class="card-header">
                <p class="card-header-title">
                  BRTA
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
              </header>
              <div class="card-content has-text-centered">
                <div class="content subtitle is-1">
                  74%
                  <p class="subtitle is-4">yyyyyyyy</p>
                </div>
              </div>
          
            </div>
      </div>

      <div class="column">
           <div class="card">
              <header class="card-header">
                <p class="card-header-title">
                  TIN
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
              </header>
              <div class="card-content has-text-centered">
                <div class="content subtitle is-1">
                  68%
                  <p class="subtitle is-4">zzzzzzzzz</p>
                </div>
              </div>
          
            </div>
      </div>

      <div class="column">
           <div class="card">
              <header class="card-header">
                <p class="card-header-title">
                  Social/Finance
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
              </header>
              <div class="card-content has-text-centered">
                <div class="content subtitle is-1">
                  50%
                  <p class="subtitle is-4">xxxxxxx</p>
                </div>
              </div>
          
            </div>
      </div>
  </div>
  

  <div class="panel-block">
   
  </div>

  
</nav>

</div>

</div>

<div id="" class="columns">
  
</div>