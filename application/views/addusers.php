<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          
        </div>

        <div class="media-content">
          <h1 class="title">Add User</h1>
          <h2 class="subtitle"> </h2>
        </div>
      </div>
    </div>

    <div class="column">
      <a class="button is-outlined is-info is-pulled-right" href="<?php echo base_url();?>index.php/users"><span class="icon is-large">
            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
          </span><span>Back to Users List</span></a>

    </div>
  </div>
</div>


<?php
if(isset($status)){
if($status != '' ){
?>
<div class="columns">
  <div class="column has-text-center" style="margin:0px 40px">
   <?php 
   if($status == "success")
   {
   ?>
    <div class="message is-success is-small has-text-centered">
      <div class="message-body">
        <p class="is-size-4" style="color:green">Entry Successful</p>
      </div>
    </div>
   <?php 
   }
   else if($status == "exists") 
   {
   ?>
    <div class="message is-warning is-small has-text-centered">
      <div class="message-body">
        <p class="is-size-4" style="color:#ffbb33">Already Exists</p>
      </div>
    </div> 
   <?php
   }
   else if($status == "failed") 
   {
   ?>  
    <div class = "failed_msg">
        <p>Failed</p>      
    </div>
   <?php 
   }
   else
   {
   ?>  
    <div class = "validation_msg">   
        <?php echo validation_errors();?>  
    </div>
     <?php
   }
   ?>  
   <?php echo validation_errors(); ?>
  </div>
  
</div>


<?php 
}}
?>


<form action="<?php echo base_url();?>index.php/users/create_user" method="post">
<nav class="panel" relative="true" style="margin: 20px;">
    <p class="panel-heading">
      User Details
    </p>

<div id="" class="columns" style="margin:0px">
  <div class="column">
    
      <div class="columns">
        <div class="column">
            <div class="field">
              <p class="control">
                <label class="label">Role</label>
                <select class="input select is-medium is-fullwidth is-danger" name="role">
                  <option>Select Role</option>
                  <option value="A">Admin</option>
                  <option value="PA">Passport Admin</option>
                </select>
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Name</label>
                <input class="input is-medium is-danger" type="text" name="name" value="" />
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Surname</label>
                <input class="input is-medium" type="text" name="surname" value="" />
              </p>
            </div>
             <div class="field">
              <p class="control">
                <label class="label">Organisation</label>
                <?php echo form_dropdown('organisation',$org_records,'',' class="select input is-danger is-medium is-fullwidth" id="org"  required="required"');?>
              </p>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <p class="control">
                <label class="label">Email</label>
                <input class="input is-medium is-danger" type="email" name="email" value="" />
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Password</label>
                <input class="input is-medium is-danger" type="password" name="password" value="" />
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Confirm Password</label>
                <input class="input is-medium is-danger" type="password" name="cpassword" value="" />
              </p>
            </div>
           
            <div class="field">
              <p class="control">
                <button class="button is-info is-large is-pulled-right" type="submit"><span class="icon is-large"><i class="fa fa-plus" aria-hidden="true"></i></span>Create User<span></span></button>
              </p>
            </div>
          </panel>
        </div>
      </div>
    
    
  </div>


</div>
</nav>
</form>

<div id="" class="columns">

</div>