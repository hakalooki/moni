<div id="mainNav">
  <div class="tabs is-vertical has-block-icons">
    <ul>
    <?php
    if (isset($this->session->userdata['logged_in']))
        $org = $this->session->userdata['logged_in']['org'];

    $urole = $this->session->userdata['logged_in']['role'];
    if($urole == 'A'): ?>
     <li><a href = "<?php echo base_url();?>index.php/auth/load_admin_dashboard"><span><i class="fa fas fa-tachometer-alt fa-2x"></i></span><br />Dashboard</a></li>
    <?php endif; ?>

     <?php if($urole == 'A' OR $urole =='PA'): ?>
     <li><a href = "<?php echo base_url();?>index.php/passport/passport_page/progress/<?=$org?>"><span><i class="fas fa-book fa-2x"></i></span><br />Passport</a></li>
     <?php endif; ?>

    <?php if($urole == 'A' OR $urole =='OA'): ?>
     <li><a href = "dash-admin.php"><span class=""><i class="fas fa-life-ring fa-2x"></i></span><br />ORG</a></li>
     <?php endif; ?>

      <?php if($urole == 'A' OR $urole =='BA'): ?>
     <li><a href = "dash-admin.php"><span><i class="fa fa-bus fa-2x"></i></span><br />BRTA</a></li>
     <?php endif; ?>

      <?php if($urole == 'A' OR $urole =='NA'): ?>
     <li><a href = "dash-admin.php"><span><i class="fa fa-snowflake fa-2x"></i></span><br />TIN</a></li>
     <?php endif; ?>

      <?php if($urole == 'A' OR $urole =='FA'): ?>
     <li><a href = "dash-admin.php"><span><i class="fa fa-tasks fa-2x"></i></span><br />Social/Finance</a></li>
     <?php endif; ?>

     <?php if($urole == 'A' OR $urole =='VA'): ?>
     <li><a href = "<?php echo base_url();?>index.php/crvs"><span><i class="fas fa-sitemap fa-2x"></i></span><br />CRVS</a></li>
     <?php endif; ?>

     <li><a href = "<?php echo base_url();?>index.php/users"><span><i class="fa fa-users fa-2x"></i></span><br />Users</a></li>
     <li><a href = "dash-admin.php"><span><i class="fa fa-bars fa-2x"></i></span><br />Reports</a></li>
     
    </ul>
  </div>
</div>