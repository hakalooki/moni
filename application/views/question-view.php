<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Questrial&display=swap" rel="stylesheet">
    <!-- Bulma Version 0.8.x-->
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.8.0/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/login.css">

    <link href = "<?php echo base_url(); ?>assets/css/bulma.min.css" rel = "stylesheet">
    <link href = "<?php echo base_url(); ?>assets/css/question.css" rel = "stylesheet">
    
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-black">জাতীয় পরিচয়পত্র জিজ্ঞাসা</h3>
                    <hr class="login-hr">
                    
                    <div class="box">
                        
                        <form>
                            <div class="field">
                              <div class="control">
                                <textarea class="textarea is-large is-focused" placeholder="your question"></textarea>
                              </div>
                            </div>

                            <button class="button is-block is-black is-large is-fullwidth">Send <i class="fas fa-file-import" aria-hidden="true"></i></button>
                        </form>
                    </div>
                    <p class="has-text-grey">
                        <a href="../">Need Help?</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script async type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>