<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          
        </div>

        <div class="media-content">
          <h1 class="title">Add Tasks</h1>
          <h2 class="subtitle"> </h2>
        </div>
      </div>
    </div>

    <div class="column">
      <a class="button is-outlined is-info is-pulled-right" href="<?php echo base_url();?>index.php/passport/passport_page/tasks/<?=$org?>"><span class="icon is-large">
            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
          </span><span>Back to Tasks List</span></a>

    </div>
  </div>
</div>


<?php 
if(isset($status)){
if($status != '' ){
?>
<div class="columns">
  <div class="column has-text-center" style="margin:0px 40px">
   <?php 
   if($status == "success")
   {
   ?>
    <div class="message is-success is-small has-text-centered">
      <div class="message-body">
        <p class="is-size-4" style="color:green">Entry Successful</p>
      </div>
    </div>
   <?php 
   }
   else if($status == "exists") 
   {
   ?>
    <div class="message is-warning is-small has-text-centered">
      <div class="message-body">
        <p class="is-size-4" style="color:#ffbb33">Already Exists</p>
      </div>
    </div> 
   <?php
   }
   else if($status == "failed") 
   {
   ?>  
    <div class = "failed_msg">
        <p>Failed</p>      
    </div>
   <?php 
   }
   else
   {
   ?>  
    <div class = "validation_msg">   
        <?php echo validation_errors();?>  
    </div>
     <?php
   }
   ?>  
   <?php echo validation_errors(); ?>
  </div>
  
</div>


<?php 
}}
?>





<div id="" class="columns">
  <div class="column data-table" style="margin:30px 40px">
    <form action="<?php echo base_url();?>index.php/passport/create_tasks" method="post">
      <div class="columns">
        <div class="column">
          <panel>
            <div class="field">
              <p class="control">
                <label class="label">Name</label>
                <input class="input is-medium" type="text" name="task_name" value="" />
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Description</label>
                <textarea class="textarea" name="desc" value=""></textarea>
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Completion Type</label>
                <select class="select is-medium is-fullwidth" name="type">
                  <option>Select Completion Type</option>
                  <option value="hs">Has Subtask</option>
                  <option value="yn">Yes/No</option>
                  <option value="pr">Percentage</option>
                </select>
              </p>
            </div>
            <div class="field">
              <p class="control">
                <label class="label">Organisation</label>
                <select class="select is-medium is-fullwidth" name="org">
                  <option>Select organisation</option>
                  <option value="1">Passport</option>
                </select>
              </p>
            </div>
            <div class="field">
              <p class="control">
                <button class="button is-info is-large is-pulled-right" type="submit"><span class="icon is-large"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>Add New<span></span></button>
              </p>
            </div>
          </panel>
        </div>
      </div>
    </form>
    
  </div>


</div>


<div id="" class="columns">

</div>