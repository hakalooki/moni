<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          <span class="icon is-large">
            <i class="fa fa-book fa-3x" aria-hidden="true"></i>
          </span>
        </div>

        <div class="media-content">
          <h1 class="title">Passport</h1>
          <h2 class="subtitle">Progress status of all passport tasks</h2>
        </div>
      </div>
    </div>

    <div class="column">
      <div class="tabbar is-fullwidth">
        <div class="tabs is-right is-boxed">
          <ul>
            <li class="is-active">
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/progress/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
                <span>Progress</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/tasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-list" aria-hidden="true"></i></span>
                <span>Tasks</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/subtasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-bars" aria-hidden="true"></i></span>
                <span>Subtasks</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>


<div id="" class="columns is-full">
  

<div id="" class="column is-12">

  <nav class="panel" relative="true" style="margin: 20px;">
    <p class="panel-heading" style="margin-bottom: 30px;">
      Task Progress
    </p>

    <?php
    //var_dump($rs);
     $tid = 0;
     $i = 0;

    foreach ($rs as $tasks):
      //$i++;
      if ($tasks->id != $tid):
        //echo 'loop '.$i;
    ?>
      <div class="columns">

          <div class="column is-4" style="margin: 0px;">
            <div class="block mt-4"><p class="title is-4"><?=$tasks->name?></p></div>
          </div>

          <div class="column is-1"></div>

          

          <?php
          if ($tasks->completion_type=='yn'):
            /*if ($tasks->progress=='y')
              $chk='checked';
            else
               $chk='';*/
          ?>
          <div class="column is-1"></div>

          <div class="column" style="margin: 0px;">
            <div class="checkbox">
              <label class="checkbox">
                <input id="<?='t'.$tasks->id?>" type="checkbox" name="" class="checkbox is-info checkx" <?php echo ($tasks->progress=='y' ? 'checked' : ''); ?>>
              </label>
            </div>
          </div>

          <?php
          elseif ($tasks->completion_type=='pr'):
          ?>

          <div class="column is-1" style="margin: 0px;">
            <div class="field">
              <div class="control">
                <input id="<?='t'.$tasks->id?>" class="input is-primary valx" type="number" value="<?=$tasks->progress?>" placeholder="">
              </div>
            </div>
          </div>

          <div class="column" style="margin: 0px;">
            <progress id="<?='pbt'.$tasks->id?>" class="progress is-success is-large" value="<?=$tasks->progress?>" max="100">60%</progress>
          </div>

          <?php
          endif;
          ?>

      </div>

    <?php
    $tid = $tasks->id;
      
    endif; 
   

    if ($tasks->sid):
    ?>

        <div class="columns">
          <div class="column is-1"></div>

          <div class="column is-3" style="margin: 0px;">
            <div class="block"><p class="subtitle is-4"><?=$tasks->subtask?></p></div>
          </div>

          <div class="column is-1"></div>

           <?php
          if ($tasks->subtask_type=='yn'):
            
          ?>

          <div class="column is-1"></div>

          <div class="column" style="margin: 0px;">
            <div class="checkbox">
              <label class="checkbox">
                <input id="<?='s'.$tasks->sid?>" type="checkbox" name="" class="checkbox is-info checkx" <?php echo ($tasks->sprogress=='y' ? 'checked' : ''); ?>>
              </label>
            </div>
          </div>

          <?php
            elseif($tasks->subtask_type=='pr'):
          ?>

          <div class="column is-1" style="margin: 0px;">
            <div class="field">
              <div class="control is-small">
                <input id="<?='s'.$tasks->sid?>" class="input is-warning valx" type="number" value="<?=$tasks->sprogress?>" placeholder="">
              </div>
            </div>
          </div>

          <div class="column" style="margin: 0px;">
            <progress  id="<?='pbs'.$tasks->sid?>" class="progress is-warning is-medium" value="<?=$tasks->sprogress?>" max="100">80%</progress>
          </div>

          <?php 
          endif;
          ?>

        </div>


    <?php 
    endif; endforeach; ?>


  

    <div class="panel-block">
     
    </div>

  
  </nav>

</div>

</div>

<div id="" class="columns">
  
</div>

<script>
  $(document).ready(function(){
    $('.valx').on('change', function(e){
      e.preventDefault();
      var tblName;
      var id=$(this).attr("id");
      var pid='#pb'+id; //progressbar id
      var oid=id.substring(1); //original id
      var preId=id.substring(0,1); //id prefix
      var pval=$(this).val();

      
      //$(pid).attr("value").val($(this).val());
      $(pid).val($(this).val());
      //alert(tblName);
      //alert($(this).val());



      if(pval == ''){
        alert('Please Enter a value');
      }
      else{

        if(preId=='t')
          tblName='tasks';
        else
          tblName='subtasks';

        $.ajax({
          url:"<?php echo base_url(); ?>index.php/passport/putProgress",
          method:"POST",
          data:{'tname':tblName,'id':oid,'progress':pval},
          success:function(data){
            //alert(data);
          }

        });
      }


    });

    $('.checkx').on('change', function(e){
      e.preventDefault();
      var tblName, cval;
      var id=$(this).attr("id");
      var oid=id.substring(1); //original id
      var preId=id.substring(0,1); //id prefix

      if($(this).is(":checked")) {
        cval="y";
        //$(this).prop('checked',true);
      } else {
        cval='n';
        //$(this).prop('checked',false);
      }
      //alert(oid);

      if(preId=='t')
          tblName='tasks';
        else
          tblName='subtasks';

        $.ajax({
          url:"<?php echo base_url(); ?>index.php/passport/putProgress",
          method:"POST",
          data:{'tname':tblName,'id':oid,'progress':cval},
          success:function(data){
            //alert(data);
          }

        });
    });

  });
</script>