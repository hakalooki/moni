<div class="page-content">

  <div class="page-title">
    <div class="columns">

      <div class="column is-5">
        <div class="media">
          <div class="media-left">
            <span class="icon is-large">
              <i class="fas fa-sitemap fa-3x" aria-hidden="true"></i>
            </span>
          </div>

          <div class="media-content">
            <h1 class="title">CRVS</h1>
            <h2 class="subtitle">All documents under your account</h2>
          </div>
        </div>
      </div>

      <div class="column">

      </div>
    </div>
  </div>

  


  <section class="section">

    <div class="columns is-centered">
      <div class="column is-6">
        <div class="panel" style="">
          <div class="panel-heading">
            <div class="level">
              <div class="level-left">
                <div class="level-item">
                  <h2 class="subtitle">Document Upload</h2>
                </div>
              </div>
              <div class="level-right">
                <div class="level-item">
                  
                  <h2 class="subtitle"><span class=""> </span></h2>
                </div>
              </div>
            </div>
          </div>

          <form method="post" id="upload_form" enctype="multipart/form-data">
          <div class="panel-block is-clearfix" style="">
            <div class="column">
              <div class="field">
                <div class="control">
                  <input class="input is-info" type="text" name="doc_title" id="doc_title" placeholder="Document Title">
                </div>
              </div>
            </div>

            <div class="column">
                <div class="file has-name is-fullwidth is-right is-warning">
                  <label class="file-label">
                    <input class="file-input is-fullwidth" type="file" name="userFile" id="userFile">
                    <span class="file-cta">
                      <span class="file-icon">
                        <i class="fas fa-upload"></i>
                      </span>
                      <!--span class="file-label"></span-->
                    </span>
                    <span class="file-name">
                      Select A Document....
                    </span>
                  </label>
                  <!--input class="button is-warning" type="submit" name="upload" id="upload" value="upload" /-->
                  <button class="button is-warning"><p>Upload</p></button>
                </div>

                <div id="uploaded_file"></div>
              
            </div>
          </div>
          </form>
        </div>
      </div>

      <div class="column is-1"></div>


      <nav class="panel">
        <p class="panel-heading">
          Documents
        </p>
        <div class="panel-block">

          <div class="column" id="docs">
            <div class="table-container">
              <table class="table">
                <tbody class="" id="docList"></tbody>
              </table>
            </div>

            <br /><br />

            <div id="uploaded_filexxxx"></div>

          </div>
        </div>
      </nav>

    </div>
  </section>
</div>

<script>
function delete_doc(e)
{
  if (confirm('Are you sure you want to delete this document?')){
      var id=$(e).data('id');
      //alert(id);
      $.ajax({
        url:"<?php echo base_url(); ?>index.php/crvs/delete_doc/"+id,
        method:"POST",
        dataType:"json",
        success:function(data){
          if (data.status === "success")
          {
            $(this).remove();
            refresh_docs();
          } else {
            alert(data.msg);
          }
        }
      });
    }
}

function refresh_docs()
{
  $.get('./crvs/get_docs/')
  .success(function (data){
    var outputs = '';
    var j=0;
    var datax=JSON.parse(data);

    /*for(var i=0; i<=JSON.parse(data).length; i++ ){
      j++;
      
      outputs+='<tr class=""><td>'
      +j+'</td><td>'
      +datax[5].title+'</td><td>'
      +datax[2].file+'</td><td><a href="#">Del</a></td></tr>';
      }*/

      /*datax.forEach(function(item){
         j++;

         outputs+='<tr class=""><td>'
         +j+'</td><td><a href="<?php //echo base_url(); ?>index.php/crvs/down_doc/'+item.file+'" target="_blank">'
         +item.title+'</a></td><td>'
         +item.file+'</td><td><a href="#">Del</a></td></tr>';
      });*/

      //var burl="<?php //echo base_url('files/cvrs/'); ?>";

      /*datax.forEach(function(item){
       j++;
       burl+=item.file;

       outputs+='<tr class=""><td>'
       +j+'</td><td><a href="'+burl+'" target="_blank">'
       +item.title+'</a></td><td>'
       +item.file+'</td><td><a href="#">Del</a></td></tr>';

       burl="<?php //echo base_url('files/cvrs/'); ?>";
      });*/

      datax.forEach(function(item){
       j++;
       burl="<?php echo base_url('files/cvrs/'); ?>";
       burl+=item.file;

       outputs+='<tr class=""><td>'
       +j+'</td><td><div class="control"><div class="tags has-addons"><a class="tag is-link is-medium" href="'+burl+'" target="_blank">'
       +item.title+'</a><a class="tag is-delete is-medium del" data-id="'+item.id+'" onclick="delete_doc(this)"></a></div></div></td><td><a href="<?php echo base_url(); ?>index.php/crvs/down_doc/'+item.file+'"><span class="icon is-link"><i class="fas fa-download"></i></span></a></td></tr>';

       
      });
      
      //console.log(Object.keys(datax).length);
      //console.log(JSON.parse(JSON.stringify(data)))
      //console.log(datax);

    $('#docList').html(outputs);
  });
}

$(document).ready(function(){
  refresh_docs();

   $('#upload_form').on('change', function(e){
    e.preventDefault();
    $('#uploaded_file').html($('#userFile').val());
   });

  
  $('#upload_form').on('submit', function(e){
    e.preventDefault();
    //

    if($('#userFile').val() == '' || $('#doc_title').val() == ''){
      alert('Please Select The File OR Give A Title For Document');
    }
    else{
      $.ajax({
        url:"<?php echo base_url(); ?>index.php/crvs/file_upload",
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
          refresh_docs();
          $('#uploaded_file').html(data);
          $('#doc_title').val('');
          $('#userFile').val('');
        }

      });
    }
  });

});
</script>