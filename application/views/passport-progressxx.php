<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          <span class="icon is-large">
            <i class="fa fa-book fa-3x" aria-hidden="true"></i>
          </span>
        </div>

        <div class="media-content">
          <h1 class="title">Passport</h1>
          <h2 class="subtitle">Progress status of all passport tasks</h2>
        </div>
      </div>
    </div>

    <div class="column">
      <div class="tabbar is-fullwidth">
        <div class="tabs is-right is-boxed">
          <ul>
            <li class="is-active">
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/progress">
                <span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
                <span>Progress</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/tasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-list" aria-hidden="true"></i></span>
                <span>Tasks</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/subtasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-bars" aria-hidden="true"></i></span>
                <span>Subtasks</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>


<div id="" class="columns is-full">
  

<div id="" class="column is-12">

  <nav class="panel" relative="true" style="margin: 20px;">
    <p class="panel-heading">
      Work Progress
    </p>
    <div class="columns">

        <div class="column is-4" style="margin: 20px;">
          <div class="block mt-4"><p class="title is-4">Collection of present agreement</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          <progress class="progress is-success is-large" value="60" max="100">60%</progress>
        </div>

    </div>


    <div class="columns">

        <div class="column is-4" style="margin: 20px;">
          <div class="block mt-4"><p class="title is-4">Combination of team members</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          <progress class="progress is-primary is-large" value="60" max="100">20%</progress>
        </div>
        
    </div>


    <div class="columns">

        <div class="column is-8" style="margin: 20px;">
          <div class="block mt-4"><p class="title is-4">Internal meeting with team members and formulation of sub task</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          
        </div>
        
    </div>


    <div class="columns">
        <div class="column is-1"></div>

        <div class="column is-4" style="margin: 20px;">
          <div class="block"><p class="subtitle is-4">Subtask 1</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          <progress class="progress is-warning is-medium" value="60" max="100">50%</progress>
        </div>
    </div>

    <div class="columns">
        <div class="column is-1"></div>

        <div class="column is-4" style="margin: 20px;">
          <div class="block"><p class="subtitle is-4">Subtask 2</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          <progress class="progress is-info is-medium" value="60" max="100">50%</progress>
        </div>
    </div>

    <div class="columns">
        <div class="column is-1"></div>

        <div class="column is-4" style="margin: 20px;">
          <div class="block"><p class="subtitle is-4">Subtask 3</p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;">
          <progress class="progress is-link is-medium" value="60" max="100">50%</progress>
        </div>
    </div>
  

    <div class="panel-block">
     
    </div>

  
  </nav>

</div>

</div>

<div id="" class="columns">
  
</div>