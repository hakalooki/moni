<div class="page-content">

  <div class="page-title">
    <div class="columns">

      <div class="column is-5">
        <div class="media">
          <div class="media-left">
            <span class="icon is-large">
              <i class="fas fa-address-card fa-3x" aria-hidden="true"></i>
            </span>
          </div>

          <div class="media-content">
            <h1 class="title">Users</h1>
            <h2 class="subtitle">All Users in the system</h2>
          </div>
        </div>
      </div>

      <div class="column">

      </div>
    </div>
  </div>


    <div class="columns" style="margin:30px">
      <div class="column">
        <form on-submit="dosearch">
          <div class="field has-addons">
            <p class="control is-expanded">
              <input class="input" type="text" placeholder="{{placeholder}}" value="{{query}}">
            </p>
              <p class="control" style="max-width:{{(1/(20 + 2))*100}}%">
                <span class="select">
                  <select value="{{filterVals[name]}}" style="max-width:100%">
                    <option value="">Any Role</option>
                    <option value="">Passport-Admin</option>
                  </select>
                </span>
              </p>


            <p class="control">
              <button class="button is-info" type="submit"><span class="icon is-small"><i class="fa fa-search" aria-hidden="true"></i></span> <span>Search</span></button>
            </p>
          </div>
        </form>
      </div>
      <div class="column is-narrow">
        <a href="<?php echo base_url();?>index.php/users/user_entry" class="button is-success is-pulled-right"><span class="icon is-small"><i class="fas fa-plus-circle"></i></span> <span>Add User</span></a>
      </div>
    </div>

    <div class="columns is-multiline"style="margin:30px">

      <?php foreach ($users as $k => $v):
       // var_dump($users); ?>
       
      <div class="column is-4">
        <div class="card">
          <div class="card-content">
            <a class="edit-icon" href="#users/view/{{id}}">
              <span class="icon is-small">
                <i class="fas fa-pencil-alt"></i>
              </span>
            </a>
            <div class="media">
              <div class="media-left">
                <figure class="image is-64x64">
                  <img src="<?php echo base_url();?>assets/img/avatar.png" alt="Image">
                </figure>
              </div>
              <div class="media-content">
                <p class="title is-4" style=""><?=$v->name?> <?=$v->surname?></p>
                <p class="subtitle is-5" style="margin-bottom:1px;margin-top:-35px"><?=$v->role?></p>
                <p><small><?=$v->email?></small></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
    <hr>
    <div class="columns">
      <div class="column">
        <pagination entityName={{page.entityName}} meta="{{meta}}"/>
      </div>
    </div>
</div>

<style>
.card-content{
  position:relative;
}
.edit-icon{
  position:absolute;
  right:5px;
  top:5px;
}
.card{
  width:100%;
}
.access-list .tag{
  margin-right:5px;
}
</style>