<!DOCTYPE html>
<html>

<?php
/*if (isset($this->session->userdata['logged_in'])) {
  $name = ($this->session->userdata['logged_in']['username']);
  $email = ($this->session->userdata['logged_in']['email']);
} else {
  header("location: http://localhost/moni/index.php/auth/user_login");
}*/

if (isset($pg)) {
  switch ($pg) {
    case "progress":
      $content_page = 'passport-progresss.php';
      break;
    case "tasks":
      $content_page = 'passport-tasks.php';
      break;
    case "subtasks":
      $content_page = 'passport-subtasks.php';
      break;
      case "addtasks":
      $content_page = 'addtasks.php';
      break;
    case "addsubtasks":
      $content_page = 'addsubtasks.php';
      break;
    default:
      $content_page = 'passport-progress.php';
  }
} else
    $content_page = 'passport-progress.php';

?>


  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Monitoring Platform</title>
    <link href = "<?php echo base_url(); ?>assets/css/bulma.min.css" rel = "stylesheet">
    <link href = "<?php echo base_url(); ?>assets/css/bulma-switch.min.css" rel = "stylesheet">
    <link href = "<?php echo base_url(); ?>assets/css/login.css" rel = "stylesheet">
    <link href = "<?php echo base_url(); ?>assets/css/style.css" rel = "stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  </head>

  <body>
    <div class="columns" style="margin: 0;">

      <!-- start Sidebar -->
      <div class="column is-narrow sidebar is-fullheight" style="width: 450px;">
        <h3 class="title is-4 logo">
          <a href = "index.php"><img src = "images/logo.png" width = "70" height = "70" /></a>
          <p class="is-size-6">Bangladesh Election Commission</p>
        </h3>

        <div class = "">
          <?php include 'sidebarx.php'; ?>
       </div> 

        <div class = "">
          <?php //include 'footer.php'; ?>
        </div>

      </div>
      <!-- end Sidebar -->


      <div id="header" class="nav">
      <div class="nav-left logo-area">
        <div class="nav-item logo-holder">
          <img class="logo" width="" height="" src="<?php echo base_url(); ?>assets/img/logoxx.png">
        </div>
        <div class="nav-item current-property-name text">
          <span>
            Monitoring Platform
          </span>
        </div>
        
      </div>

      <div class="nav-right is-flex-mobile">
        <div class="nav-item text">
          <div class="media">
            <div class="media-left">
              
                <figure class="image is-32x32"><img src=""></figure>
              
            </div>
            <div class="media-content">
              <span class="is-block" style="line-height:1.3rem"><?php echo $this->session->userdata['logged_in']['name'].' '. $this->session->userdata['logged_in']['surname'];?></span>
              <small class="is-block"><span class="tag is-warning"><a href="#">My Profile</a></span>&nbsp;|&nbsp;<span class="tag is-dark"><?php echo $this->session->userdata['logged_in']['role'];?></span></small>
            </div>
          </div>
        </div>
        <div class="nav-item">
          <a class="button is-danger" href="<?php echo base_url();?>index.php/auth/logout">
            <span class="icon"><i class="fab fa-github"></i></span>
            <span class="is-hidden-mobile">Logout</span></a>
        </div>
      </div>
    </div>

    <div id="contentArea">
      <?php
      include $content_page;
      //include 'passport-progress.php';
      ?>
    </div>


      <div class="column has-text-centered dashboard">
         <h4 class="title has-text-red" style="">xx xxxx xxxxxx</h4>
      </div>
    </div>
  </body>
</html>