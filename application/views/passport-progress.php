<?php
if (isset($this->session->userdata['logged_in'])) {
  $org = $this->session->userdata['logged_in']['org'];
}
?>

<div class="page-title">
  <div class="columns">

    <div class="column is-5">
      <div class="media">
        <div class="media-left">
          <span class="icon is-large">
            <i class="fa fa-book fa-3x" aria-hidden="true"></i>
          </span>
        </div>

        <div class="media-content">
          <h1 class="title">Passport</h1>
          <h2 class="subtitle">Progress status of all passport tasks</h2>
        </div>
      </div>
    </div>

    <div class="column">
      <div class="tabbar is-fullwidth">
        <div class="tabs is-right is-boxed">
          <ul>
            <li class="is-active">
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/progress/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
                <span>Progress</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/tasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-list" aria-hidden="true"></i></span>
                <span>Tasks</span>
              </a>
            </li>
            <li>
              <a href = "<?php echo base_url();?>index.php/passport/passport_page/subtasks/<?=$org?>">
                <span class="icon is-small"><i class="fas fa-bars" aria-hidden="true"></i></span>
                <span>Subtasks</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>


<div id="" class="columns is-full">
  

<div id="" class="column is-12">

  <nav class="panel" relative="true" style="margin: 20px;">
    <p class="panel-heading">
      Task Progress
    </p>

    <?php
    //var_dump($rs);
     $tid = '';
     $i = 0;

    foreach ($rs as $tasks):
      $i++;
      if (is_null($tasks->sid)):
        echo 'loop '.$i;

    ?>
      <div class="columns">

          <div class="column is-4" style="margin: 20px;">
            <div class="block mt-4"><p class="title is-4"><?=$tasks->name?></p></div>
          </div>

          <div class="column is-1"></div>

          <div class="column is-1" style="margin: 20px;">
            <div class="field">
              <div class="control">
                <input class="input is-primary" type="text" value="<?=$tasks->progress?>" placeholder="">
              </div>
            </div>
          </div>

          <?php
          if ($tasks->completion_type=='yn'):
            /*if ($tasks->progress=='y')
              $chk='checked';
            else
               $chk='';*/
          ?>

          <div class="column" style="margin: 20px;">
            <div class="checkbox">
              <label class="checkbox">
                <input id="" type="checkbox" name="" class="checkbox is-rounded is-info" <?php echo ($tasks->progress=='y' ? 'checked' : ''); ?>>
              </label>
            </div>
          </div>

          <?php
          else:
          ?>

          <div class="column" style="margin: 20px;">
            <progress class="progress is-success is-large" value="<?=$tasks->progress?>" max="100">60%</progress>
          </div>

          <?php
          endif;
          ?>

      </div>

    <?php
      
    endif; 
   

    if (!is_null($tasks->sid)):
      if ($tid == ''):
        $tid = $tasks->id;
    ?>


      <div class="columns">
        <div class="column is-4" style="margin: 20px;">
          <div class="block mt-4"><p class="title is-4"><?=$tasks->name?></p></div>
        </div>

        <div class="column is-1"></div>

        <div class="column" style="margin: 20px;"></div>
      </div>

    

    <?php endif;
    if ($tid == $tasks->id):
    ?>

        

        <div class="columns">
          <div class="column is-1"></div>

          <div class="column is-4" style="margin: 20px;">
            <div class="block"><p class="subtitle is-4"><?=$tasks->subtask?></p></div>
          </div>

          <div class="column is-1"></div>

          <div class="column is-1" style="margin: 20px;">
            <div class="field">
              <div class="control">
                <input class="input is-warning" type="text" value="<?=$tasks->sprogress?>" placeholder="">
              </div>
            </div>
          </div>

          <div class="column" style="margin: 20px;">
            <progress class="progress is-warning is-medium" value="<?=$tasks->sprogress?>" max="100">80%</progress>
          </div>
        </div>


    <?php else:
            $tid = '';
          endif; 
    //else $tid = '' ;
    endif; endforeach; ?>


  

    <div class="panel-block">
     
    </div>

  
  </nav>

</div>

</div>

<div id="" class="columns">
  
</div>