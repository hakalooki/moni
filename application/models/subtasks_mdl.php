<?php

Class Subtasks_mdl extends CI_Model {

	public function listAllByOrg($org) {

		//$cond = "mobile =" . "'" . $mobileOrMail . "' OR email ="."'".$mobileOrMail . "'";
		//$cond = "organisation ="."'".$org . "'";
		$this->db->select('s.*, t.name AS task, o.name AS org');
		$this->db->from('subtasks s');
		$this->db->join('tasks t', 't.id=s.task', 'left');
		$this->db->join('organisations o', 't.organisation=o.id');
		$this->db->where('t.organisation', $org);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function listAllSelectByOrg($table_name,$value_field,$option_field,$org)
	{
		$query = $this->db->from($table_name)
							-> where('organisation', $org)
							-> get();
		
		$select[''] = 'Select A '.$option_field;
		if($query->num_rows() > 0){
			foreach($query->result() as $field){
				$select[$field->$value_field] = $field->$option_field;
			}
		}
		return $select;
	}

	function create() {
		$timezone = "Asia/Dhaka";
		date_default_timezone_set($timezone);
		$bd_date=date('Y-m-d');
		
		$data = array(		
			'name' => $this -> input ->post('subtask_name'),
			'description' => $this -> input ->post('desc'),
			'task' => $this -> input ->post('task'),
			'completion_type' => $this -> input ->post('type'),
			'created' => $bd_date,
			'modified' => $bd_date
		);
		$this -> db -> insert('subtasks', $data);
		
		return true;
	}

}

?>