<?php

Class Tasks_mdl extends CI_Model {

	public function listAllByOrg($org) {

		//$cond = "mobile =" . "'" . $mobileOrMail . "' OR email ="."'".$mobileOrMail . "'";
		//$cond = "organisation ="."'".$org . "'";
		$this->db->select('t.*, o.name AS org');
		$this->db->from('tasks t');
		$this->db->join('organisations o', 't.organisation=o.id');
		//$this->db->join('subtasks s', 't.id=s.task', 'left');
		$this->db->where('t.organisation', $org);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function redundancy_check($table, $field, $item)
	{
		$query = $this -> db -> select( $field )
							 -> from( $table )
							 -> get();
		 $temp_new = strtolower( preg_replace('/\s+/', '', $item));
		 foreach($query -> result() as $info):
			$temp_old = strtolower( preg_replace('/\s+/', '',$info -> $field));
			if($temp_old == $temp_new) return true;
		 endforeach;
		 
		 return false;
	}

	function create() {
		$timezone = "Asia/Dhaka";
		date_default_timezone_set($timezone);
		$bd_date=date('Y-m-d');
		
		$data = array(		
			'name' => $this -> input ->post('task_name'),
			'description' => $this -> input ->post('desc'),
			'completion_type' => $this -> input ->post('type'),
			'organisation' => $this -> input ->post('org'),
			'created' => $bd_date,
			'modified' => $bd_date
		);
		$this -> db -> insert('tasks', $data);
		
		return true;
	}

	public function putProgress($data) {
	    extract($data);
	    $this->db->where('id', $id);
	    $this->db->update($table_name, array('progress' => $progress));
	    return true;
	}

	public function taskProgressByOrg($org) {

		$this->db->select('t.*, s.id AS sid, s.task, s.name AS subtask, s.progress AS sprogress, s.completion_type AS subtask_type');
		$this->db->from('tasks t');
		$this->db->join('subtasks s', 's.task=t.id', 'left');
		$this->db->where('t.organisation', $org);
		$this->db->order_by('t.id');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function taskProgressByOrgxxxx($org) {

		$this->db->select('tp.*, t.name AS task, s.name AS subtask, t.completion_type AS task_type, s.completion_type AS subtask_type');
		$this->db->from('task_progress tp');
		$this->db->join('tasks t', 'tp.task=t.id');
		$this->db->join('subtasks s', 'tp.subtask=s.id', 'left');
		$this->db->where('t.organisation', $org);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

}

?>