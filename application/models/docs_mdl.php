<?php
class Docs_mdl extends CI_Model {
 
	public function listAll() {
	  return $this->db->select()
	          ->from('documents')
	          ->get()
	          ->result();
	}
 
	function create($fname) {
		$timezone = "Asia/Dhaka";
		date_default_timezone_set($timezone);
		$bd_date=date('Y-m-d');
		
		$data = array(		
			'title' =>$_POST['doc_title'],
			'file' => $fname
			//'created' => $bd_date,
			//'modified' => $bd_date
		);
		$this -> db -> insert('documents', $data);
		
		return true;
	}

	public function delete_doc($id) {
	  $doc = $this->get_doc($id);
	  if (!$this->db->where('id', $id)->delete('documents'))
	  {
	  	return FALSE;
	  }
	  unlink('./files/cvrs/' . $doc->file);    
	  return TRUE;
	}
	 
	public function get_doc($id) {
	  return $this->db->select()
	          ->from('documents')
	          ->where('id', $id)
	          ->get()
	          ->row();
	}
}
?>