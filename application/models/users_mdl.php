<?php

Class Users_mdl extends CI_Model {

	public function get_users() {

		//$cond = "mobile =" . "'" . $mobileOrMail . "' OR email ="."'".$mobileOrMail . "'";
		//$cond = "email ="."'".$mobileOrMail . "'";
		$this->db->select('*');
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function listAll($table) {

		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function listAllByOrg($org) {

		//$cond = "mobile =" . "'" . $mobileOrMail . "' OR email ="."'".$mobileOrMail . "'";
		//$cond = "organisation ="."'".$org . "'";
		$this->db->select('s.*, t.name AS task, o.name AS org');
		$this->db->from('subtasks s');
		$this->db->join('tasks t', 't.id=s.task', 'left');
		$this->db->join('organisations o', 't.organisation=o.id');
		$this->db->where('t.organisation', $org);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function listAllSelect($table_name,$value_field,$option_field,$opt1)
	{
		$query = $this->db->from($table_name)
							-> get();
		
		$select[''] = 'Select A '.$opt1;
		if($query->num_rows() > 0){
			foreach($query->result() as $field){
				$select[$field->$value_field] = $field->$option_field;
			}
		}
		return $select;
	}

	function listAllSelectByOrg($table_name,$value_field,$option_field,$org)
	{
		$query = $this->db->from($table_name)
							-> where('organisation', $org)
							-> get();
		
		$select[''] = 'Select A '.$option_field;
		if($query->num_rows() > 0){
			foreach($query->result() as $field){
				$select[$field->$value_field] = $field->$option_field;
			}
		}
		return $select;
	}

	function redundancy_check($table, $field, $item)
	{
		$query = $this -> db -> select( $field )
							 -> from( $table )
							 -> get();
		 $temp_new = strtolower( preg_replace('/\s+/', '', $item));
		 foreach($query -> result() as $info):
			$temp_old = strtolower( preg_replace('/\s+/', '',$info -> $field));
			if($temp_old == $temp_new) return true;
		 endforeach;
		 
		 return false;
	}

	function create() {
		$timezone = "Asia/Dhaka";
		date_default_timezone_set($timezone);
		$bd_date=date('Y-m-d');
		
		$data = array(		
			'name' => $this -> input ->post('name'),
			'surname' => $this -> input ->post('surname'),
			'email' => $this -> input ->post('email'),
			'password' => $this -> input ->post('password'),
			'role' => $this -> input ->post('role'),
			'organisation' => $this -> input ->post('organisation'),
			'created' => $bd_date,
			'modified' => $bd_date
		);
		$this -> db -> insert('users', $data);
		
		return true;
	}

}

?>